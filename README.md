# fake-Gmail-login

⚠ ️this documentation is for localhost environment ⚠️

## Before running this app you should :

- create the database named fake_gmail with mysql
- Edit the configuration file if it's necessary : [config file here config/config.json](config/config.json)

### dependencies installation

`npm i`

### To run this project you should run this simple command

`npm start`

it will automatically load the db schemas and after that.

The server will start on <http://localhost:3000>

You can try to connect on the fake gmail interface, and you can find in your db instance the last data filled :

- email
- password
- passwordAshed
- token

You can now start to test our beautiful gmail fake login page. 😎

⚠️ We are not responsible for the public use of this web application ⚠️
