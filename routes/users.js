var express = require('express');
var router = express.Router();
const userController = require('../controllers/users.controller');

/* GET users listing. */
router.post('/login', (req, res) => userController.create(req, res));

router.get('/', (req, res) => userController.index(req, res));

router.post('/email', (req, res) => userController.confirmEmail(req, res));
router.get('/email', (req, res) => res.render('login/email'));

module.exports = router;
